
import os
import re

from pprint import pprint

from config import *
from util import *


def process_cexec_command(config, args):

    if len(args) == 0:
        print("Help info")

    subcmd = args.pop(0)

    try:
        if subcmd == 'help':
            print("""
help
list
create
remove
exec add
exec del
vol add
vol del
var/env add
var/env del
image
relink
            """)
        if subcmd == 'list':
            print("\n".join(group_names()))

        elif subcmd == 'create':
            group = args.pop(0)
            cexec_create_group(group, args)

        elif subcmd == 'remove':
            group = args.pop(0)
            delete_config(group)

        elif subcmd == 'exec':
            group = args.pop(0)
            data = read_config(group)

            if len(args) == 0 and not(config['exec_options']['add'] or config['exec_options']['del']):
                for exec_name in data['executables']:
                    print(exec_name)
                sys.exit(0)

            # additions/deletions on subcommand line
            if len(args) > 0:
                config['exec_options']['add'] = []
                config['exec_options']['del'] = []
                while len(args) >=2:
                    operation = args.pop(0)
                    if re.match(r'add', operation):
                        config['exec_options']['add'].append(args.pop(0))
                    elif re.match(r'(del|delete|rm|remove)', operation):
                        config['exec_options']['del'].append(args.pop(0))

            if config['exec_options']['add']:
                for exec_name in config['exec_options']['add']:
                    # Add exec_name to yaml config
                    data['executables'].append(exec_name)
                    # Add link into bin directory for exec name
                    link_exec_name(exec_name)

            if config['exec_options']['del']:
                for exec_name in config['exec_options']['del']:
                    print("deleting {}".format(exec_name))
                    # Remove exec_name from yaml config
                    data['executables'].remove(exec_name)
                    # Remove link from bin directory for exec name
                    unlink_exec_name(exec_name)

            write_config(group, data)

        elif subcmd == 'vol':
            group = args.pop(0)
            data = read_config(group)

            if len(args) == 0 and not(config['exec_options']['add'] or config['exec_options']['del']):
                print("Container path\tOS path")
                for container_path,os_path in data['volumes'].items():
                    print("{}\t\t{}".format(container_path, os_path))
                sys.exit(0)

            # additions/deletions on subcommand line
            if len(args) > 0:
                config['exec_options']['add'] = []
                config['exec_options']['del'] = []
                while len(args) >=2:
                    operation = args.pop(0)
                    if re.match(r'add', operation):
                        config['exec_options']['add'].append(args.pop(0))
                    elif re.match(r'(del|delete|rm|remove)', operation):
                        config['exec_options']['del'].append(args.pop(0))

            if config['exec_options']['add']:
                for volume_spec in config['exec_options']['add']:
                    try:
                        os_path, container_path =  re.split(r'[:=]', volume_spec)
                    except ValueError:
                        print("ERROR: Invalid volume specification")
                        print("       Host path and container path need = or : delimiter")
                        sys.exit(1)
                    if container_path in data['volumes'].keys():
                        print("WARNING: Container path ({}) already exists; NOT adding".format(container_path))
                    else:
                        data['volumes'][container_path] = os_path

            if config['exec_options']['del']:
                for volume in config['exec_options']['del']:
                    if volume in data['volumes'].keys():
                        del(data['volumes'][volume])
                    else:
                        print("WARNING: Coontainer volume ({}) not found".format(volume))

            write_config(group, data)

        elif subcmd == 'var' or subcmd == 'env':
            group = args.pop(0)
            data = read_config(group)

            if len(args) == 0 and not(config['exec_options']['add'] or config['exec_options']['del']):
                print("Env var\t\tValue")
                for var,val in data['env'].items():
                    print("{}\t\t{}".format(var, val))
                sys.exit(0)

            # additions/deletions on subcommand line
            if len(args) > 0:
                config['exec_options']['add'] = []
                config['exec_options']['del'] = []
                while len(args) >=2:
                    operation = args.pop(0)
                    if re.match(r'add', operation):
                        config['exec_options']['add'].append(args.pop(0))
                    elif re.match(r'(del|delete|rm|remove)', operation):
                        config['exec_options']['del'].append(args.pop(0))

            if config['exec_options']['add']:
                for var_spec in config['exec_options']['add']:
                    try:
                        var,val =  re.split(r'[:=]', var_spec)
                    except ValueError:
                        print("ERROR: Invalid variable specification")
                        print("       Variable name and value need = or : delimiter")
                        sys.exit(1)
                    if var in data['env'].keys():
                        print("WARNING: Env variable ({}) already exists; NOT adding".format(var))
                    else:
                        data['env'][var] = val

            if config['exec_options']['del']:
                for var in config['exec_options']['del']:
                    if var in data['env'].keys():
                        del(data['env'][var])
                    else:
                        print("WARNING: Env variable ({}) not found".format(var))

            write_config(group, data)

        elif subcmd == 'image':
            group = args.pop(0)
            data = read_config(group)

            if len(args) == 0:
                try:
                    image = data['container']['image']
                    tag = data['container']['tag']
                except KeyError:
                    print("WARNING: Image not set on {}".format(group))
                    sys.exit(0)

                print("{}:{}".format(image, tag))
                sys.exit(0)
            else:
                image = args.pop(0)
                if ':' in image:
                    image,tag = image.split(':')
                else:
                    tag = "latest"
                data['container']['image'] = image
                data['container']['tag'] = tag

            write_config(group, data)

        elif subcmd == 'relink':
            groups = []
            if len(args) == 0:
                groups = group_names()
            else:
                groups.append(args.pop(0))

            for group in groups:
                relink_executables(group)

        elif subcmd == 'edit':
            group = args.pop(0)

            editor = 'vi'
            if 'EDITOR' in os.environ:
                editor = os.environ['EDITOR']

            os.system("{} {}".format(editor, config_file_path(group)))

            # relink the executables in case one was added
            relink_executables(group)

        else:
            print("Unknown cexec subcommand")
            return 1
    except UnboundLocalError:
        print("Execution group needs to be specified")
        return 1

    return 0


def cexec_create_group(name, args):
    template = {
        'name': name,
        'executables': [],
        'container': {},
        'env': {},
        'volumes': {}
    }

    if len(args) == 1:
        # image specified on create command
        image = args.pop(0)
        if ':' in image:
            image,tag = image.split(':')
        else:
            tag = "latest"
        template['container']['image'] = image
        template['container']['tag'] = tag

    write_config(name, template)
