import os
import re

BASE_DIR = ""

def set_base_dir(prog):
    global BASE_DIR

    # strip program name
    prog_dir = re.sub(r'\/\w+$', '', prog)
    BASE_DIR = os.path.dirname(prog_dir)

def config_dir():
    return os.path.join(BASE_DIR, 'etc')

def bin_dir():
    return os.path.join(BASE_DIR, 'bin')

def config_file_path(name):
    if not name.endswith('.yaml'):
        name += ".yaml"
    return os.path.join(config_dir(), name)

def link_exec_name(name):
    cexec_filename = os.path.join(bin_dir(), 'cexec')
    try:
        os.link(cexec_filename, os.path.join(bin_dir(), name))
    except FileExistsError:
        print("WARNING: {} already exists; NOT adding".format(name))

def unlink_exec_name(name):
    try:
        os.unlink(os.path.join(bin_dir(), name))
    except FileNotFoundError:
        print("WARNING: {} not found".format(name))

def add_switch(sw, args):
    result = []
    for val in args:
        result.append(sw)
        result.append(val)
    return result
