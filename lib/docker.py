
import os
import subprocess
import re

from util import *

def check_docker_image(image, tag='latest'):
    proc = subprocess.Popen(['docker', 'images'], stdout=subprocess.PIPE)
    stdout = proc.communicate()

    for line in stdout[0].decode("utf-8").split("\n"):
        if re.match(r'%s\s+%s\b' % (image, tag), line):
            return True
    return False

def download_docker_image(image, tag='latest'):
    proc = subprocess.Popen(['docker', 'pull', '%s:%s' % (image, tag)])
    proc.communicate()

def volume_expansion(vol_str):
    vol_str = os.path.expanduser(vol_str)
    vol_str = os.path.expandvars(vol_str)
    # vol_str = re.sub(r"\$\{?PWD\}?", os.getcwd(), vol_str)
    # vol_str = re.sub(r"\$\{?HOME\}?", os.environ['HOME'], vol_str)
    # vol_str = re.sub(r"~", os.environ['HOME'], vol_str)
    return vol_str

def execute(prog, config, args):
    volumes = ['%s:%s' % (volume_expansion(config['volumes'][dir]), dir)
               for dir in config.get('volumes', [])]
    env_vars = ['%s=%s' % (var, config['env'][var]) for var in config.get('env', [])]
    ports = config.get('ports', [])

    image = '%s:%s' % (config['container']['image'], config['container']['tag'])

    if config['exec_options']['verbose']:
        print(' '.join(['docker', 'run', '-it', '--rm'] +
                       add_switch('-e', env_vars) +
                       add_switch('-v', volumes) +
                       add_switch('-p', ports) +
                       [image, prog] + args))
    proc = subprocess.Popen(['docker', 'run', '-it', '--rm'] +
                            add_switch('-e', env_vars) +
                            add_switch('-v', volumes) +
                            add_switch('-p', ports) +
                            [image, prog] + args)
    proc.communicate()
